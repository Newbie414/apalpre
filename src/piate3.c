/*
 * piate3.c
 *
 *  Created on: 16. 10. 2017
 *      Author: testing
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv) {

	FILE *fin1, *fin2;
	char c1, c2;
	int flag1 = 0;
	int flag2 = 0;
	fin1 = fopen("prvy.txt", "r");
	fin2 = fopen("druhy.txt", "r");

	if (fin1 != NULL || fin2 != NULL) {

		c1 = fgetc(fin1);
		c2 = fgetc(fin2);
		while (c1 != EOF && c2 != EOF) {
			if (c1 != c2) {
				flag1++;
			}
			c1 = fgetc(fin1);
			c2 = fgetc(fin2);
		}

		while (c1!= EOF || c2 != EOF) {
			flag2++;
			c1 = fgetc(fin1);
			c2 = fgetc(fin2);
		}
	}

	else {
		printf("Chyba pri otvarani suborov\n");
		return 0;

	}

	if (flag1 == 0 && flag2 == 0) {
		printf("Subory su identicke\n");
	} else {
		printf("Subory sa lisia v %d znakoch a jeden zo suborov je dlhsi o %d",
				flag1, flag2);
	}

	return 0;
}

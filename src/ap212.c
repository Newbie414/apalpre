/*
 * ap21.c
 *
 *  Created on: 14. 3. 2017
 *      Author: testing
 */

/* ******************************************************************************** */
/*                                                                                  */
/*  Architektura pocitacov - Uloha 2.1                                              */
/*                                                                                  */
/*  Program inkrementuje v assembleri premennu                                      */
/*                                                                                  */
/*  Autor: Richard Balogh <balogh@elf.stuba.sk>                                     */
/*  Historia:                                                                       */
/*             2.3.2006 zakladna verzia funkcna                                     */
/*             6.3.2006 verzia pre oba kompilatory                                  */
/*  Prenositelnost:                                                                 */
/*            Zalezi na syntaxi prekladaca!                                         */
/* ******************************************************************************** */
/*
#include <stdio.h>

static int c1;         // Niektore verzie potrebuju globalne premenne

int main(int argc, char* argv[]) {
	printf("Zadaj cislo: ");
	scanf("%d", &c1);

#ifdef __GNUC__                      // Tato cast sa preklada len v Dev-C++ (gcc)

	 asm(".intel_syntax noprefix \n"   // Prepneme z AT&T syntaxe na na Intel

	       "mov eax,c1        \n"   // iCislo -> EAX
	       "shl eax                \n"   // EAX ++
	       "mov c1,eax     \n"   // EAX  -> iVysledok

	       ".att_syntax            \n"); // Dame vsetko do povodneho stavu


#else
	printf("Nepodporovaný kompilátor");
	/*
	 /*
	 #elif _MSC_VER                       // Tato cast sa preklada iba v MS Visual C++

	 __asm {                          // zaciatok bloku asm
	 MOV EAX, iCislo                  // do EAX vloz hodnotu premennej iCislo (z pamate)
	 INC EAX                           // pripocitaj 1
	 MOV iVysledok,EAX                // do premennej iVysledok vloz vysledok z registra EAX
	 }                                // koniec bloku asm
	 */

/*
#endif

	printf("Vysledok je: %d", c1);
	return (0);
}
*/

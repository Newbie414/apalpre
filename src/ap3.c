/*
 * main.c
 *
 *  Created on: 19. 4. 2017
 *      Author: testing
 */
/*
#include <windows.h>
#include <stdio.h>
#include "ncurses/ncurses.h"

boolean otvor_port(HANDLE hCom, DCB dcbSerialParams, COMMTIMEOUTS timeouts);
boolean prints(HANDLE hSerial, char *string, int bytes);

int main() {
	char array[255], c;
	char array_position=0;
	HANDLE hSerial;
	DCB dcbSerialParams = { 0 };
	COMMTIMEOUTS timeouts = { 0 };

	// Open the highest available serial port number
	fprintf(stderr, "Opening serial port...");
	hSerial = CreateFile("\\\\.\\COM2", GENERIC_READ | GENERIC_WRITE, 0, NULL,
	OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hSerial == INVALID_HANDLE_VALUE) {
		fprintf(stderr, "Error\n");
		return 1;
	} else
		fprintf(stderr, "OK\n");

	// Set device parameters (38400 baud, 1 start bit,
	// 1 stop bit, no parity)
	dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
	if (GetCommState(hSerial, &dcbSerialParams) == 0) {
		fprintf(stderr, "Error getting device state\n");
		CloseHandle(hSerial);
		return 1;
	}

	dcbSerialParams.BaudRate = CBR_38400;
	dcbSerialParams.ByteSize = 8;
	dcbSerialParams.StopBits = ONESTOPBIT;
	dcbSerialParams.Parity = NOPARITY;
	if (SetCommState(hSerial, &dcbSerialParams) == 0) {
		fprintf(stderr, "Error setting device parameters\n");
		CloseHandle(hSerial);
		return 1;
	}

	// Set COM port timeout settings
	timeouts.ReadIntervalTimeout = 50;
	timeouts.ReadTotalTimeoutConstant = 50;
	timeouts.ReadTotalTimeoutMultiplier = 10;
	timeouts.WriteTotalTimeoutConstant = 50;
	timeouts.WriteTotalTimeoutMultiplier = 10;
	if (SetCommTimeouts(hSerial, &timeouts) == 0) {
		fprintf(stderr, "Error setting timeouts\n");
		CloseHandle(hSerial);
		return 1;
	}

	//otvor_port(&hSerial,&dcbSerialParams,timeouts);

	initscr();						//inicializácia grafického režimu
	if (has_colors() == FALSE) {
		printw("Chyba pri použivaní farieb\n");
		getch();
		return 0;
	}

	move(getmaxy(stdscr)-1, 0);
	while (1) {
		c = getch();
		if (c == 0) {
			c = getch();
			if (c == 27) {
				break;
			}
		}
		if (c == '\n') {
			for(char i=0;i<=array_position;i++){
				printw(" ");
			}
			move(getmaxy(stdscr)-1, 0);
			array[array_position]='\0';
			prints(hSerial, array, strlen(array));
		}
		array[array_position] = c;
		array_position++;
	}

	move(0, 0);
	printw("\nPole je: %s", array);

	// Close serial port
	fprintf(stderr, "Closing serial port...");
	if (CloseHandle(hSerial) == 0) {
		fprintf(stderr, "Error\n");
		return 1;
	}
	fprintf(stderr, "OK\n");

	// exit normally
	return 0;
}
/*
 boolean otvor_port(HANDLE hSerial, DCB *dcbSerialParams, COMMTIMEOUTS timeouts) {
 // Open the highest available serial port number
 fprintf(stderr, "Opening serial port...");
 hSerial = CreateFile("\\\\.\\COM2", GENERIC_READ | GENERIC_WRITE, 0, NULL,
 OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
 if (hSerial == INVALID_HANDLE_VALUE) {
 fprintf(stderr, "Error\n");
 return 1;
 } else
 fprintf(stderr, "OK\n");

 // Set device parameters (38400 baud, 1 start bit,
 // 1 stop bit, no parity)
 dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
 if (GetCommState(hSerial, &dcbSerialParams) == 0) {
 fprintf(stderr, "Error getting device state\n");
 CloseHandle(hSerial);
 return 1;
 }

 dcbSerialParams.BaudRate = CBR_38400;
 dcbSerialParams.ByteSize = 8;
 dcbSerialParams.StopBits = ONESTOPBIT;
 dcbSerialParams.Parity = NOPARITY;
 if (SetCommState(hSerial, &dcbSerialParams) == 0) {
 fprintf(stderr, "Error setting device parameters\n");
 CloseHandle(hSerial);
 return 1;
 }

 // Set COM port timeout settings
 timeouts.ReadIntervalTimeout = 50;
 timeouts.ReadTotalTimeoutConstant = 50;
 timeouts.ReadTotalTimeoutMultiplier = 10;
 timeouts.WriteTotalTimeoutConstant = 50;
 timeouts.WriteTotalTimeoutMultiplier = 10;
 if (SetCommTimeouts(hSerial, &timeouts) == 0) {
 fprintf(stderr, "Error setting timeouts\n");
 CloseHandle(hSerial);
 return 1;
 }
 return 0;
 }

 */
/*
boolean prints(HANDLE hSerial, char *string, int bytes) {
	// Send specified text (remaining command line arguments)
	DWORD bytes_written;
	fprintf(stderr, "Sending bytes...");
	if (!WriteFile(hSerial, &bytes, 5, &bytes_written, NULL)) {
		fprintf(stderr, "Error\n");
		CloseHandle(hSerial);
		return 1;
	}
	fprintf(stderr, "%d bytes written\n", (int) bytes_written);
	return 0;
}

*/

/*
 * ap23.c
 *
 *  Created on: 14. 3. 2017
 *      Author: testing
 */
/* ******************************************************************************** */
/*                                                                                  */
/*  Architektura pocitacov - Uloha 2.1                                              */
/*                                                                                  */
/*  Program inkrementuje v assembleri premennu                                      */
/*                                                                                  */
/*  Autor: Richard Balogh <balogh@elf.stuba.sk>                                     */
/*  Historia:                                                                       */
/*             2.3.2006 zakladna verzia funkcna                                     */
/*             6.3.2006 verzia pre oba kompilatory                                  */
/*  Prenositelnost:                                                                 */
/*            Zalezi na syntaxi prekladaca!                                         */
/* ******************************************************************************** */
/*
#include <stdio.h>

static int c1;         // Niektore verzie potrebuju globalne premenne

int main(int argc, char* argv[]) {

	printf("Zadaj znak: ");
	scanf("%d", &c1);

	asm(".intel_syntax noprefix \n"  // Prepneme z AT&T syntaxe na na Intel

			"mov eax,c1				\n"// iCislo -> EAX
			"cmp eax,9				\n"
			"jg hexacislo			\n"
			"mov ebx,48				\n"
			"add eax,ebx			\n"// EAX ++
			"mov c1,eax				\n"// EAX  -> iVysledok
			"jmp end				\n"
			"hexacislo:				\n"
			"mov ebx,55				\n"
			"add eax,ebx			\n"// EAX ++
			"mov c1,eax				\n"// EAX  -> iVysledokF
			"end:					\n"

			".att_syntax            \n");
	// Dame vsetko do povodneho stavu

	printf("Vysledok je: %c",c1);
	return (0);
}


*/

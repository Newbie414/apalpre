/*
 * ap28.c
 *
 *  Created on: 15. 4. 2017
 *      Author: testing
 */

/*
 * Predpokladajme ze raid4 pole je p�skovan� po 8 bitoch teda znakoch a
 * �e po�koden� disk je ten 2h�
 *
 * RAID 4 funguje na princ�pe kontroln�ho s��tu diskov a t�m vznik� opravn� disk v tomto pr�pade je to 4t� disk.
 * Be�ne sa pou��va na vytvorenie kontroln�ho s��tu XOR lebo je to primit�vna funkcia a z�rove� aj r�chla.
 * Teda ak je na prvom disku zap�sane ahoj na druhom disku je zap�san� svet. Tak sa ka�d� jeden bit na n-tej poz�ci XORuje s n-t�m b�tom na inom disku
 */
/*
 #include "stdio.h"
 #include "stdlib.h"

 int main(int argc, char **argv) {

 FILE *disk1, *disk2, *disk3, *disk4;
 char c1, c3, c4;

 disk1 = fopen("file01.txt", "r");//otv�ranie diskov na ��tanie
 disk2 = fopen("file02.txt", "w");//do druh�ho disku budeme zapisova� lebo je po�koden�
 disk3 = fopen("file03.txt", "r");
 disk4 = fopen("file04.txt", "r");

 while ((c1 = fgetc(disk1)) != EOF && (c3 = fgetc(disk3)) != EOF && (c4 =
 fgetc(disk4)) != EOF)
 //Podmienka overuje �i fgetc nevr�ti End Of File teda, �e s�bor sa skon�il. Ak sa aspo� jeden zo s�borov skon�il nem�m �al�ie d�ta z ktor�ch by som mohol vypo��ta� kontroln� s��et
  {
 fprintf(disk2, "%c", (c1 ^ c3 ^ c4)); //Znak ^ v C znamen� XOR oper�tor teda fprintf() zap�e do s�boru ktor� je prv�m parametrom teda disk2 pod�a form�tu v druhom parametry to �o je v tre�om parametri teda zap�e XOR 1,3,4 disku teda v�sledkom je 2-h� disk
 }

 }
 */

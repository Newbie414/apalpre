/*
 * ap25.c
 *
 *  Created on: 15. 3. 2017
 *      Author: testing
 */
#include "stdio.h"

/*
 * Ukazovate� v�dy m� v sebe ulo�en� nejak� hodnotu ktor� reprezentuje adresu premennej v pamati. Pam�t ma najmen�� blok jeden bajt.
 */
/*

int main(int argc, char **argv) {
	char pole[10];
	char *ukazovatel;			// Vytvor�m si ukazovate� na premenn� typu char. Je d�le�it� aby sa zhodoval d�tov� typ na ktor� ukazovate� ukazuje s d�tov�m typom premennej na ktor� ukazuje
	printf("Zadaj retazec: ");	// Ak by sa nezhodovali tak pri po�it� pr�kazu ukazovatel++ by sa zv��ila adresa na ktor� ukazuje o in� po�et bajtov ako m�. Teda ak by sme mali ukazovate� na typ int kotr� na zv��a 32 bitov teda 4 bajty. Po�iato�n� adresa na ktor� ukazuje = 0x00 (0x znamn�, �e ide o ��slo v 16kovej s�stave) po pr�kaze ukazovatel++ = 0x004. �o by bol 4-t� znak v poli typu char.
	scanf("%s", pole);			//Na��tam do po�a
	ukazovatel = pole;			//Nastav�m adresu na ktor� ukazuje na adresu kde sa za��na pole teda na adresu jeho prv�ho prvku. Prv� prvok ma napr. adresu 0x00 druh� 0x01 at�.
	printf("\nUkazovatel na pole je: %p a premenna ma adresu %p", pole, ukazovatel);	//Porovnanie �i naozaj ukazuje na rovnak� adresu ako je adresa pola.
	ukazovatel++;				//Inkrementuje adresu ukazovatela o velkost d�tov�ho typu ako sme mu ur�ili. V tomto pr�pade ma char jeden bajt a teda adresa sa zv��ila o 1
	*ukazovatel = 'X';			//Na adresu na ktor� ukazuje ukazovate� zap�eme znak X. V tomto pr�pade to je druh� prvok pola
	printf("\nA teraz je ukazovatel: %p",ukazovatel);
	printf("\nPole je teraz: %s", pole);

	return 0;
}



*/
